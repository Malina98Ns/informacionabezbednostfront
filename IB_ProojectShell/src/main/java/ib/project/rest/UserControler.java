package ib.project.rest;


import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import ib.project.model.Authority;
import ib.project.model.User;
import ib.project.service.AuthorityService;
import ib.project.service.UserService;

@RestController
@RequestMapping(value="korisnici")
public class UserControler {
	

	@Autowired
	public AuthorityService authorityService;
	@Autowired
	public UserService userService;	
	
	
	@GetMapping(path="/")
	public ArrayList<User> findAll() {
		
		return userService.findAll();
	}
	
	
	
	

	
	
	@PostMapping(path="korisnik/login")
	public ResponseEntity<User> loginUser(@RequestParam String email, @RequestParam String password) {
		
		User user = userService.findByEmailAndPassword(email, password);
		try {
			
			return new ResponseEntity<User>(user, HttpStatus.OK);
		} catch (Exception e) {
			
			System.out.println("Nema korisnika sa tim mejlom u bazi podataka!");
			
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping(path="korisnik/registration")
	public ResponseEntity<User> registrationUser(@RequestParam String email, @RequestParam String password) {
		
		Authority auth = authorityService.findByName("Regular");
		User user = new User();
		User checkUser = userService.findByEmail(email);
		if (checkUser == null) {
			
			
			user.setAuthority(auth);
			
			user.setEmail(email);
			user.setPassword(password);
			userService.save(user);
			
			return new ResponseEntity<User>(user,HttpStatus.CREATED);
		}else {
			System.out.println("Mejl vec postoji u bazi podataka!");
			
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}
	}
	
		
}